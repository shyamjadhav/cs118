import uk.ac.warwick.dcs.maze.logic.IRobot;

/**
 * @author tailoredtech
 *
 *This Exercise is about to find optimized solution to find ways to target
 */

public class Exercise_19
{
	//from this function we can get total no of exits available
	private int availableWays(IRobot robot)
	{
		int availableExitsCount = 0;
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) != IRobot.WALL)
			{
				availableExitsCount++;
			}
		}
		return availableExitsCount;
	}
	
	// when robot is at  cross_road or junction
	private int atThe_crossRoadandjunction(IRobot robot)
	{
		int possibleDirection;
		int availableExits = toCheckNon_WallSides(robot);
		int randomNum;
		if (availableExits == 0)
			possibleDirection = anyWayToExitsFromNonWall(robot);
		else
		{	do
		{
			randomNum = gettingRandomNo(0, 3);
			possibleDirection = IRobot.AHEAD+randomNum;

		}while(robot.look(possibleDirection) != IRobot.PASSAGE);
		}
		return possibleDirection;
	}

	// main method to run.
	public void controlRobot(IRobot robot)
	{
		int line;
		// getting ways on the basis total no of ways available.
		int waysToExits = availableWays(robot);
		switch(waysToExits)
		{
		case 1 : 
			line = atTheDead_Ends(robot);// only one way available for Dead_Ends
			break;
		case 2 : 
			line = atThe_Corridor(robot);// two ways available at Corridor 
			break;
		default : 
			line = atThe_crossRoadandjunction(robot);//four or three  ways are available
			break;
		}
		// facing the robot at obtained direction
		robot.face(line);  
		// moving the robot
		robot.advance();
	}

	//getting count of non wall side of robot which is currently facing 
	private int toCheckNon_WallSides(IRobot robot)
	{
		int non_WallSides = 0;
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) == IRobot.PASSAGE)
				non_WallSides++;
		}
		return non_WallSides;
	}

	// when robot is at DeadEnd;
	private int atTheDead_Ends(IRobot robot)
	{
		int possibleWay;

		if(robot.getLocationX() == 1 && robot.getLocationY() == 1)
			possibleWay = anyWayToExitsFromNonWall(robot);
		else
			possibleWay = IRobot.BEHIND;

		return possibleWay;
	}

	
	//when robot is at corridor
	private int atThe_Corridor(IRobot robot)
	{
		int getPossibleDirection = IRobot.AHEAD;

		if (robot.look(IRobot.AHEAD) != IRobot.WALL)
			getPossibleDirection = IRobot.AHEAD;

		else if (robot.look(IRobot.LEFT) != IRobot.WALL)
			getPossibleDirection = IRobot.LEFT;

		else if (robot.look(IRobot.RIGHT) != IRobot.WALL)
			getPossibleDirection = IRobot.RIGHT;

		return getPossibleDirection;
	}

	
	// get any way to exit non wall
	private int anyWayToExitsFromNonWall(IRobot robot)
	{
		int possibleDirection;
		int randNo;
		do
		{
			randNo = gettingRandomNo(0,3);
			possibleDirection = IRobot.AHEAD+randNo;
		}while(robot.look(possibleDirection)==IRobot.WALL);
		return possibleDirection;
	}

	//getting the random no within the range
	private int gettingRandomNo(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}
}