import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_12
{
	private int randNo;
	private int line;

	public void controlRobot(IRobot robot)
	{
		isTargetNorth(robot);
		do
		{
			//Select a random number
			randNo = randomNumber(0, 3);
			
			// Convert this to a direction
			if (randNo == 0)
				line = IRobot.LEFT;
			else if (randNo == 1)
				line = IRobot.RIGHT;
			else if (randNo == 2)
				line = IRobot.BEHIND;
			else 
				line = IRobot.AHEAD;
			
		}while(robot.look(line)==IRobot.WALL);
	
		//keeps choosing a direction till it finds a non wall one
		// Face the robot in this direction
		
		robot.face(line);  
		// Move the robot
		robot.advance();
	
	}
	// Generates random number within range min-max (boundaries inclusive)
	private int randomNumber(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}

	private byte isTargetNorth(IRobot robot)
	{
		// returning 1 for ‘yes’, -1 for ‘no’ and 0 for ‘same latitude’
		if (robot.getLocationY() > robot.getTargetLocation().y )
		{
			System.out.println("Target is in the north direction.");
			return 1;
		}
		else if (robot.getLocationY() < robot.getTargetLocation().y )
		{
			System.out.println("Target is in the south direction.");
			return -1;
		}
		else
		{
			System.out.println("Target is at same level.");
			return 0;
		}
	}
}