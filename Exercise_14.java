import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_14
{
	private int randNum;
	private int line;
	private int fromNorth;
	private int fromEast;

	private int isTargetNorth(IRobot robot)
	{
		//returning the 1 ,0,-1 for north, south and same level respectively.
		if (robot.getLocationY() > robot.getTargetLocation().y )
		{
			System.out.println("Robots Target is in the north direction.");
			return 1;
		}
		else if (robot.getLocationY() < robot.getTargetLocation().y )
		{
			System.out.println("Robots Target is in the south direction.");
			return -1;
		}
		else
		{
			System.out.println("Robots Target is at same level.");
			return 0;
		}
	}

	private int isTargetEast(IRobot robot)
	{
		//returning the 1 ,0,-1 for east, west and same latitude respectively.
		if (robot.getLocationX() > robot.getTargetLocation().x )
		{
			System.out.println("Robots Target is in the East direction.");
			return 1;
		}
		else if (robot.getLocationX() < robot.getTargetLocation().x )
		{
			System.out.println("Robots Target is in the West direction.");
			return -1;
		}
		else
		{
			System.out.println("Robots Target is at same level.");
			return 0;
		}
	}

	
	public void controlRobot(IRobot robot)
	{
		fromNorth = isTargetNorth(robot);
		fromEast = isTargetEast(robot);
		if( robot.getHeading() == IRobot.NORTH )
		{
			controllerToNorth(robot);
		}
		robot.face(line);  
		robot.advance();
	}
	//getting random num.
	private int randomNumber(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}

	//controller
	private void controllerToNorth(IRobot robot)
	{
		do
		{
			if(fromEast == 0)
			{
				line = IRobot.AHEAD + ( fromNorth - 1 );
			}
			else
			{
				randNum = randomNumber(0, 1);
				switch(randNum)
				{
				case 0: 
					line = IRobot.BEHIND - fromEast;
					break;
				default:	 
					line = IRobot.BEHIND - fromNorth -1;
				}
			}
		}while(robot.look(line)==IRobot.WALL);
	}
}
