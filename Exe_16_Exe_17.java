import uk.ac.warwick.dcs.maze.logic.IRobot;

/**
 * @author tailoredtech
 *   four controller are design to optimize 
 */

public class Exe_16_Exe_17
{
	private int randNo;
	private int line;
	private int fromNorth;
	private int fromEast;
	private int turnLeftRight;
	private int moveFarwardackward;
	//private int toSouth;
	//private int toNorth;


	private int isTargetNorth(IRobot robot)
	{
		//returning the 1 ,0,-1 for north, south and same level respectively.
		if (robot.getLocationY() > robot.getTargetLocation().y )
		{
			System.out.println("Robots Target is in the north line.");
			return 1;
		}
		else if (robot.getLocationY() < robot.getTargetLocation().y )
		{
			System.out.println("Robots Target is in the south line.");
			return -1;
		}
		else
		{
			System.out.println("Robots Target is at same level.");
			return 0;
		}
	}

	private int isTargetEast(IRobot robot)
	{
		//returning the 1 ,0,-1 for east, west and same level respectively.
		if (robot.getLocationX() > robot.getTargetLocation().x )
		{
			System.out.println("Robots Target is in the East line.");
			return 1;
		}
		else if (robot.getLocationX() < robot.getTargetLocation().x )
		{
			System.out.println("Robots Target is in the West line.");
			return -1;
		}
		else
		{
			System.out.println("Robots Target is at same level.");
			return 0;
		}
	}


	public void controlRobot(IRobot robot)
	{

		fromNorth = isTargetNorth(robot);
		fromEast = isTargetEast(robot);

		// getting proper line on the basis of head 
		if( robot.getHeading() == IRobot.NORTH )
			controllerNorth(robot);
		if( robot.getHeading() == IRobot.SOUTH )
			controllerSouth(robot);
		if( robot.getHeading() == IRobot.EAST )
			controllerEast(robot);
		if( robot.getHeading() == IRobot.WEST )
			controllerWest(robot);


		turningRobot(robot);
		// Face the robot in this line
		robot.face(line);  
		// Move the robot
		robot.advance();
	}

	//getting random number in the range of  min-max 
	private int randomNumber(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}

	// towards north 
	private void controllerNorth(IRobot robot)
	{
		turnLeftRight = -fromEast;
		moveFarwardackward = fromNorth;
	}

	//  towards South
	private void controllerSouth(IRobot robot)
	{
		turnLeftRight = fromEast;
		moveFarwardackward = -fromNorth;
	}

	// towards east 
	private void controllerEast(IRobot robot)
	{
		turnLeftRight = fromNorth;
		moveFarwardackward = fromEast;
	}

	// towards west 
	private void controllerWest(IRobot robot)
	{
		turnLeftRight = -fromNorth;
		moveFarwardackward = -fromEast;
	}

	private void turningRobot(IRobot robot)
	{

		do
		{
			//equality of  random no.
			randNo = randomNumber(0, 1);

			// Convert this to a line
			switch(randNo)
			{
			case 0: 	// turning
				if (turnLeftRight == 0)
				{
					line = IRobot.AHEAD + Math.abs( moveFarwardackward -1 );
				}
				else
				{
					line = IRobot.AHEAD + (turnLeftRight+2);
				}
				break;
				//moving
			default:
				if(moveFarwardackward == 0)
				{
					line = IRobot.AHEAD + (turnLeftRight+2);
				}
				else
				{
					line = IRobot.AHEAD + Math.abs( moveFarwardackward -1 );
				}
			}
		}while(robot.look(line)==IRobot.WALL);
	}
}