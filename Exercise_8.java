import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_8
{
	int side;
	String head;
	int line;
	int randno;
	int leftCount,rightCount,ehindCount, aheadCount;

	public void controlRobot(IRobot robot) 
	{
		head="";
		line=IRobot.AHEAD;
		while(robot.look(line)==IRobot.WALL)
		{
			//randno = (int) (Math.random()*3);
			randno = (int) Math.floor(Math.random()*3);
			System.out.println("randno: "+randno);

			if (randno == 0)
			{
				line = IRobot.LEFT;
				head = "left";
				leftCount++;
			}
			else if (randno == 1)
			{
				line = IRobot.RIGHT;
				head = "right";
				rightCount++;
			}
			else if (randno == 2)
			{
				line = IRobot.BEHIND;
				head = "ackwards";
				ehindCount++;
			}
			else {
				line = IRobot.AHEAD;
				head = "forward";
				aheadCount++;
			}
		}
		robot.face(line); 
		// line=IRobot.AHEAD;
		locationDetection(robot);
		System.out.println("Summary of moves: Forward="+aheadCount+" Left="+leftCount+" Right="+rightCount+" Backwards="+ehindCount+"");
		robot.advance(); 
	}

	private void locationDetection(IRobot robot) 
	{
		side=0;

		for(int i=0; i<4; i++)
		{
			if(robot.look(IRobot.AHEAD+i)==IRobot.WALL)
			{
				side++;
			}
		}

		switch (side)
		{
			case 0: head+=" at cross road";
			break;
			case 1: head+=" at a junction";
			break;
			case 2: head+=" down corridor";
			break;
			default:head+=" at a deadend";
		}	
	}
}