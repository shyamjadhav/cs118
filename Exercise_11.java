import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_11
{
	
	int randno;
	int line;

	public void controlRobot(IRobot robot) 
	{
		// Select a random number
		randno = (int) Math.round(Math.random()*3);
		// Convert this to a direction
		do
		{
			randno = (int) Math.round(Math.random()*3);
			
			if (randno == 0)
				line = IRobot.LEFT;
			else if (randno == 1)
				line = IRobot.RIGHT;
			else if (randno == 2)
				line = IRobot.BEHIND;
			else 
				line = IRobot.AHEAD;

		}
		//keeps choosing a direction till it finds a non wall one		
		while(robot.look(line)==IRobot.WALL);
		/* Face the robot in this direction */
		robot.face(line); 
		/* and move the robot */   
		robot.advance();  
		
	}
}