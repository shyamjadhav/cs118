import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_5
{
	int side;
	String head;
	int line;
	int randno;

	public void controlRobot(IRobot robot) 
	{
		head="";
		do
		{
			randno = (int) Math.round(Math.random()*3);
			if (randno == 0)
			{
				line = IRobot.LEFT;
				head = "left";
				//break;
			}
			else if (randno == 1)
			{
				line = IRobot.RIGHT;
				head = "right";
				//break;
			}
			else if (randno == 2)
			{
				line = IRobot.BEHIND;
				head = "ackwards";
				//break;
			}
			else {
				line = IRobot.AHEAD;
				head = "forward";
			}
		}while(robot.look(line)==IRobot.WALL);
		robot.face(line); 
		locationDetection(robot);
		System.out.println("I am going  "+head);
		robot.advance(); 
	}

	private void locationDetection(IRobot robot) 
	{
		side=0;

		for(int i=0; i<4; i++)
		{
			if(robot.look(IRobot.AHEAD+i)==IRobot.WALL)
			{
				side++;
			}
		}

		switch (side)
		{
			case 0: head+=" at cross road";
			break;
			case 1: head+=" at a junction";
			break;
			case 2: head+=" down corridor";
			break;
			default:head+=" at a deadend";
		}	
	}
}