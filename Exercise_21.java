import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_21
{
	// step count in every step
	private int robotRunning = 0;	
	private LogsOfRobot_21 robotlogs;
	//  zero for  backtrack and one  for explorer;
	private int mode;	

	public void controlRobot(IRobot robot)
	{
		int direction;

		if ( (robot.getRuns() == 0) && (robotRunning == 0))
		{
			// when new maze selected on first movement of first running
			robotlogs = new LogsOfRobot_21();
			mode = 1;
		}
		if (mode == 1)	// first time meeting path is availale
		{
			direction = controllingExplore(robot);
		}
		else // No first time meeting path is availale
		{
			direction = controllingbacktrack(robot);
		}	

		robot.face(direction);  		// facing the robot at obtained direction
		robot.advance();		// moving the robot
		robotRunning++;			// Incrementing
	}

	// Controller for exploring mode
	public int controllingExplore(IRobot robot)
	{
		int line;
		// getting ways on the basis total no of ways available.
		int exits = availableWays(robot);

		// switching the path or way  on the asis of path availale at present location
		switch(exits)
		{
		case 1 : 
			line = atTheDead_Ends(robot);// only one way available for Dead_Ends
			break;
		case 2 : 
			line = atThe_Corridor(robot);// two ways available at Corridor 
			break;
		default : 
			line = atThe_crossRoadandjunction(robot);//four or three  ways are available
			break;
		}
		return line;
	}

	// Controller for backtracking mode
	public int controllingbacktrack(IRobot robot)
	{
		int line;
		// getting ways on the basis total no of ways available.
		int exits = availableWays(robot);

		// switching the path or way  on the asis of path availale at present location
		switch(exits)
		{
		case 1 : 
			line = atTheDead_Ends(robot); // only one way available for Dead_Ends
			break;
		case 2 : 
			if (toCheckNon_WallSides(robot) != 0)
				mode =1;		// first time meeting path is availale
			line = atThe_Corridor(robot); // two ways available at Corridor
			break;
		default : 
			line = atThe_crossRoadandjunction(robot);//four or three  ways are available
		}
		return line;
	}

	//from this function we can get total no of exits available
	private int availableWays(IRobot robot)
	{
		int availableExitsCount = 0;

		// from this loop we get availale path count
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) != IRobot.WALL)
			{
				availableExitsCount++;//incrementing
			}
		}
		return availableExitsCount;
	}

	//getting count of non wall side of robot which is currently facing
	private int toCheckNon_WallSides(IRobot robot)
	{
		int non_WallSides = 0;
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) == IRobot.PASSAGE)
			{
				non_WallSides++;//incrementing passage count.
			}
		}
		return non_WallSides;
	}

	// getting the exact no when robot was previously visited at the current position
	private int exitsOnbeenbefore(IRobot robot)
	{
		int beenbeforeCount = 0;

		// here we checking  for  no of previously visited  exits adjacent to current posiotion
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) == IRobot.BEENBEFORE)
			{
				beenbeforeCount++;
			}
		}
		return beenbeforeCount;
	}

	// when robot is at DeadEnd;
	private int atTheDead_Ends(IRobot robot)
	{
		int possibleDirection = IRobot.AHEAD;

		if (robotRunning == 0)
		{	

			for(int i = 1; i<=3 && ( robot.look(possibleDirection)==IRobot.WALL) ; i++)
			{
				possibleDirection = IRobot.AHEAD+i;//incrementing  
			}
		}
		else
		{	
			mode = 0;// Turning 
			possibleDirection = IRobot.BEHIND;
		}
		return possibleDirection;
	}

	//when robot is at corridor
	private int atThe_Corridor(IRobot robot)
	{
		int possibleDirection = IRobot.AHEAD;

		if (robot.look(IRobot.AHEAD) != IRobot.WALL)
		{

			possibleDirection = IRobot.AHEAD;//  AHEAD
		}
		else if (robot.look(IRobot.LEFT) != IRobot.WALL)
		{
			possibleDirection = IRobot.LEFT;	// turning LEFT
		}
		else if (robot.look(IRobot.RIGHT) != IRobot.WALL)
		{
			possibleDirection = IRobot.RIGHT; // turning RIGHT
		}
		return possibleDirection;
	}

	// when robot is at  cross_road or junction
	private int atThe_crossRoadandjunction(IRobot robot)
	{
		int possibleDirection = IRobot.AHEAD;
		int exits = toCheckNon_WallSides(robot);
		int count  = exitsOnbeenbefore(robot);
		int randomNo;

		if (count == 1)	// junction visiting first time
		{
			robotlogs.recordJunction(robot.getLocationX(), robot.getLocationY(), robot.getHeading());
		}

		if (exits == 0)
			mode = 0;	//controlacktracking
		else
			mode = 1;	// first time meeting path is availale

		if (mode == 0)		
		{
			int cameFrom = robotlogs.findlocation(robot.getLocationX(), robot.getLocationY());
			int towords;

			// repeating
			switch(cameFrom)
			{
			case IRobot.NORTH :
				towords = IRobot.SOUTH ;
				break;
			case IRobot.EAST :
				towords = IRobot.WEST;
				break;
			case IRobot.SOUTH :
				towords = IRobot.NORTH;
				break;
			default :
				towords = IRobot.EAST;
			}
			robot.setHeading(towords);
		}
		else
		{
			do
			{
				randomNo = gettingRandomNo(0, 3);
				possibleDirection = IRobot.AHEAD+randomNo;
			}while(robot.look(possibleDirection) != IRobot.PASSAGE);
		}

		return possibleDirection;
	}

	// called when reset button pressed 
	public void refresh()
	{
		robotRunning = 0;
		mode = 1;
		robotlogs.resetJunctionCounter();
	}

	//getting the random no within the range
	private int gettingRandomNo(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}
}

/**
 * This class stores log of visited junctions
 */
class LogsOfRobot_21
{
	private static int totalJunctionAllow = 10000;
	private static int visitedJuncNo;		
	private static LogsOfJunction logsJunction[];

	// inner class
	private class LogsOfJunction
	{
		private int xJunction;
		private int yJunction;
		private int cameFrom;	

		//inner class constructor
		public LogsOfJunction(int locX, int locY, int fronting)
		{
			xJunction = locX;
			yJunction = locY;
			cameFrom = fronting;
		}
	}
	//constructor
	public LogsOfRobot_21()
	{
		visitedJuncNo = 0;
		logsJunction = new LogsOfJunction [totalJunctionAllow];
	}

	// Resets visited junction counter to 0 after each run
	public void resetJunctionCounter()
	{
		visitedJuncNo = 0;
	}

	// here we getting the already visited  Junction
	public void recordJunction(int xPos, int yPos, int heading)
	{
		logsJunction[visitedJuncNo] = new LogsOfJunction(xPos, yPos, heading);
		showJunction();
		visitedJuncNo++;
	}

	/// Prints log of current junction robot is visiting
	public void showJunction()
	{
		String fronting = "";

		switch(logsJunction[visitedJuncNo].cameFrom)
		{
		case IRobot.NORTH :	fronting = "NORTH";
		break;
		case IRobot.EAST :	fronting = "EAST";
		break;
		case IRobot.SOUTH :	fronting = "SOUTH";
		break;
		case IRobot.WEST :	fronting = "WEST";
		break;
		}
		//printing the logs of junction
		System.out.print(" Position X = "+ logsJunction[visitedJuncNo].xJunction);
		System.out.print( ", Position Y = " + logsJunction[visitedJuncNo].yJunction);
		System.out.print(" Junction Visited " + (visitedJuncNo+1) );
		System.out.print(" fronting  " + fronting);
		System.out.println();

	}

	//here we are searching the location of junction
	public int findlocation(int locX, int locY)
	{
		for(int i = 0; i<visitedJuncNo; i++)	// each junction who already visited
		{
			if ( (logsJunction[i].xJunction == locX) && (logsJunction[i].yJunction == locY) )
			{	
				return logsJunction[i].cameFrom;
			}
		}
		//if record not found
		return -1;	
	}
}