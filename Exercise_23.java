import uk.ac.warwick.dcs.maze.logic.IRobot;


/**
 * @author tailoredtech
 * in this exercise we finding the route in Depth-first search in path finding.
 */
public class Exercise_23
{
	// step count in every step
	private int robotRunning = 0;		
	private LogsOfRobot_23 robotLogs;
	//  zero for  backtrack and one  for explorer;
	private int mode;	

	//when robot is at corridor
	private int atThe_Corridor(IRobot robot)
	{
		int possibleDirection = IRobot.AHEAD;

		if (robot.look(IRobot.AHEAD) != IRobot.WALL)
		{
			possibleDirection = IRobot.AHEAD;
		}
		else if (robot.look(IRobot.LEFT) != IRobot.WALL)
		{
			possibleDirection = IRobot.LEFT;
		}
		else if (robot.look(IRobot.RIGHT) != IRobot.WALL)
		{
			possibleDirection = IRobot.RIGHT;
		}
		return possibleDirection;
	}

	// when robot is at  cross_road or junction
	private int atThe_crossRoadandjunction(IRobot robot)
	{
		int possibleDirection = IRobot.AHEAD;
		int availableExits = toCheckNon_WallSides(robot); //getting the non wall sides
		int count = exitsOnbeenbefore(robot);
		int randomNo;
		if (count == 1)	
		{
			robotLogs.junctionLogs(robot.getHeading());
		}

		if (availableExits == 0)
			mode = 0;	
		else
			mode = 1;	

		if (mode == 0)		
		{
			int getFrom = robotLogs.findingJunction();
			int towords;

			switch(getFrom)
			{
			case IRobot.NORTH :
				towords = IRobot.SOUTH ;
				break;
			case IRobot.EAST :
				towords = IRobot.WEST;
				break;
			case IRobot.SOUTH :
				towords = IRobot.NORTH;
				break;
			case IRobot.WEST :
				towords = IRobot.EAST;
				break;
			default :
				possibleDirection = IRobot.CENTRE;
				towords = IRobot.EAST;
			}
			robot.setHeading(towords);
		}
		else
		{
			do
			{
				randomNo = gettingRandomNo(0, 3);
				possibleDirection = IRobot.AHEAD+randomNo;
			}while(robot.look(possibleDirection) != IRobot.PASSAGE);
		}

		return possibleDirection;
	}

	// Controller for exploring mode
	public int controllingExplore(IRobot robot)
	{
		int line;
		int exits = availableWays(robot);

		switch(exits)
		{
		case 1 :
			line = atTheDead_Ends(robot);
			break;
		case 2 : 
			line = atThe_Corridor(robot);
			break;
		default :
			line = atThe_crossRoadandjunction(robot);
			break;
		}
		return line;
	}

	// Controller for backtrack mode
	public int controllingbacktrack(IRobot robot)
	{
		int line;
		int exits = availableWays(robot);
	
		// switching the path or way  on the asis of path availale at present location
		switch(exits)
		{
		case 1 :
			line = atTheDead_Ends(robot);// only one way available for Dead_Ends
			break;
		case 2 :
			if (toCheckNon_WallSides(robot) != 0)
				mode =1;
			line = atThe_Corridor(robot);// two ways available at Corridor 
			break;
		default : 
			line = atThe_crossRoadandjunction(robot);//four or three  ways are available
		}
		return line;
	}

	//from this function we can get total no of exits available
	private int availableWays(IRobot robot)
	{
		int availableExitsCount = 0;
		// from this loop we get availale path count 
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) != IRobot.WALL)
			{
				availableExitsCount++;
			}
		}
		return availableExitsCount;
	}

	//main method to run robot
	public void controlRobot(IRobot robot)
	{
		int line;

		if ( (robot.getRuns() == 0) && (robotRunning == 0))
		{
			// when new maze selected on first movement of first running
			robotLogs = new LogsOfRobot_23();
			mode = 1;
		}
		if (mode == 1)		// first time meeting path is availale		
		{
			line = controllingExplore(robot);
		}
		else						
		{
			line = controllingbacktrack(robot);
		}	
		if(line != IRobot.CENTRE)		
		{
			robot.face(line);  		
			robot.advance();	
		}
		robotRunning++;			
	}

	//getting count of non wall side of robot which is currently facing 
	private int toCheckNon_WallSides(IRobot robot)
	{
		int non_WallSides = 0;

		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) == IRobot.PASSAGE)
			{
				non_WallSides++; //incrementing passage count.
			}
		}
		return non_WallSides;
	}

	// getting the exact no when robot was previously visited at the current position
	private int exitsOnbeenbefore(IRobot robot)
	{
		int beenbeforeCount = 0;

	   	// here we checking  for  no of previously visited  exits adjacent to current posiotion
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) == IRobot.BEENBEFORE)
			{
				beenbeforeCount++;
			}
		}
		return beenbeforeCount;
	}

	// when robot is at DeadEnd;
	private int atTheDead_Ends(IRobot robot)
	{
		int possibleWay = IRobot.AHEAD;

		if (robotRunning == 0)
		{	
			for(int i = 1; i<=3 && ( robot.look(possibleWay)==IRobot.WALL) ; i++)
			{
				possibleWay = IRobot.AHEAD+i;
			}
		}
		else
		{	
			mode = 0;
			possibleWay = IRobot.BEHIND;
		}
		return possibleWay;
	}

	// called when reset button pressed 
	public void refresh()
	{
		robotRunning = 0;
		mode = 1;
		robotLogs.refreshJunction();
	}

	//getting the random no within the range
	private int gettingRandomNo(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}
}

/**

 * @author tailoredtech 
 * Printing the logs of robot 
 */
class LogsOfRobot_23
{
	private static int visitedJuncNo;		
	private static int totalJunctionAllow = 5000;
	private int[] theWayFromRobotCameToJunction;
	// the Way From Robot Came To Junction

	//here we are searching the location of junction
	public int findingJunction()
	{
		if(visitedJuncNo <= 0)		// here all junctions are visited		
			return -1;				
		else
		{
			visitedJuncNo--;
			return theWayFromRobotCameToJunction[visitedJuncNo];	
		}
	}
	//showing the current junction
	public void junctionLogs(int fronting)
	{
		theWayFromRobotCameToJunction [visitedJuncNo] = fronting;
		visitedJuncNo++;
	}
	//constructor
	public LogsOfRobot_23()
	{
		visitedJuncNo = 0;
		theWayFromRobotCameToJunction = new int [totalJunctionAllow];
	}

	// reseting the junction from initial
	public void refreshJunction()
	{
		visitedJuncNo = 0;
	}
}