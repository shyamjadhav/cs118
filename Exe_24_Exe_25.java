import uk.ac.warwick.dcs.maze.logic.IRobot;

/**
 * @author tailoredtech
 * here we add two new method i.e beenbeforeExits,controllingExplore
 *  finding the loops  
 */
public class Exe_24_Exe_25
{
	// step count in every step
	private int robotRunning = 0;		
	private LogsOfRobot_25 robotData;
	//  zero for  backtrack and one  for explorer;
	private int modes;	

	public void controlRobot(IRobot robot)
	{
		int line;
		if ( (robot.getRuns() == 0) && (robotRunning == 0))
		{
			// when new maze selected on first movement of first running
			robotData = new LogsOfRobot_25();
			modes = 1;
		}

		if (modes == 1)	// first time meeting path is availale
		{
			System.out.println("explore");
			line = controllingExplore(robot);
		}
		else	// No first time meeting path is availale
		{
			System.out.println("acktrack");
			line = controllingbacktrack(robot);
		}	

		if(line != IRobot.CENTRE)		
		{
			robot.face(line);  		// facing the robot at obtained direction
			robot.advance();		// moving the robot
		}
		robotRunning++;			// Incrementing the steps counter
	}

	// Controller for exploring mode
	public int controllingExplore(IRobot robot)
	{
		int line;
		// getting ways on the basis total no of ways available.
		int exits = availableWays(robot);

		// switching the path or way  on the asis of path availale at present location 
		switch(exits)
		{
		case 1 : 
			line = atTheDead_Ends(robot);// only one way available for Dead_Ends
			break;
		case 2 :  
			line = atThe_Corridor(robot);// two ways available at Corridor
			break;
		default : //four or three  ways are available
			if ( exitsOnbeenbefore(robot) > 1)
			{
				line = atTheDead_Ends(robot);   // turning ack ecause of loop 			
			}
			else
				line = atThe_crossRoadandjunction(robot);	
		}
		return line;
	}

	// Controller for backtracking mode
	public int controllingbacktrack(IRobot robot)
	{
		int line;
		int exits = availableWays(robot);
		switch(exits)
		{
		case 1 : 
			line = atTheDead_Ends(robot);// only one way available for Dead_Ends
			break;
		case 2 : 
			if (toCheckNon_WallSides(robot) != 0)// two ways available at Corridor

				modes =1;			// first time meeting path is availale
			line = atThe_Corridor(robot);
			break;
		default :  //four or three  ways are available
			line = atThe_crossRoadandjunction(robot);	
		}
		return line;
	}

	//from this function we can get total no of exits available
	private int availableWays(IRobot robot)
	{
		int availableExitsCount = 0;

		// from this loop we get availale path count 
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) != IRobot.WALL)
			{
				availableExitsCount++;
			}
		}

		return availableExitsCount;
	}

	//getting count of non wall side of robot which is currently facing
	private int toCheckNon_WallSides(IRobot robot)
	{
		int non_WallSides = 0;
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) == IRobot.PASSAGE)
			{
				non_WallSides++;  //incrementing passage count.
			}
		}
		return non_WallSides;
	}

	// getting the exact no when robot was previously visited at the current position
	private int exitsOnbeenbefore(IRobot robot)
	{
		int beenbeforeCount = 0;

		// here we checking  for  no of previously visited  exits adjacent to current posiotion
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) == IRobot.BEENBEFORE)
			{
				beenbeforeCount++;
			}
		}
		return beenbeforeCount;
	}

	// when robot is at DeadEnd;
	private int atTheDead_Ends(IRobot robot)
	{
		int optimalDirection = IRobot.AHEAD;

		if (robotRunning == 0)
		{	
			for(int i = 1; i<=3 && ( robot.look(optimalDirection)==IRobot.WALL) ; i++)
			{
				optimalDirection = IRobot.AHEAD+i;
			}
		}
		else
		{	// getting turn;
			modes = 0;
			optimalDirection = IRobot.BEHIND;
		}
		return optimalDirection;
	}

	//when robot is at corridor
	private int atThe_Corridor(IRobot robot)
	{
		int possibleDirection = IRobot.AHEAD;

		if (robot.look(IRobot.AHEAD) != IRobot.WALL)
		{
			possibleDirection = IRobot.AHEAD;//ahead turn
		}
		else if (robot.look(IRobot.LEFT) != IRobot.WALL)
		{
			possibleDirection = IRobot.LEFT; //turning left
		}
		else if (robot.look(IRobot.RIGHT) != IRobot.WALL)
		{
			possibleDirection = IRobot.RIGHT;// turning right
		}
		return possibleDirection;
	}

	// when robot is at  cross_road or junction
	private int atThe_crossRoadandjunction(IRobot robot)
	{
		int possibleDirection = IRobot.AHEAD;
		int availableExits = toCheckNon_WallSides(robot);
		int count  = exitsOnbeenbefore(robot);
		int randomNo;

		if (count == 1)	// save head coz meeting first time
		{
			robotData.junctionLogs(robot.getHeading());
		}
		if (availableExits == 0)
			modes = 0;		// first time meeting path is availale
		else
			modes = 1;	// No first time meeting path is availale

		if (modes == 0)		
		{
			int arrivedFrom = robotData.findingJunction();
			int towords;

			switch(arrivedFrom)
			{
			case IRobot.NORTH :
				towords = IRobot.SOUTH ;
				break;
			case IRobot.EAST :
				towords = IRobot.WEST;
				break;
			case IRobot.SOUTH :
				towords = IRobot.NORTH;
				break;
			case IRobot.WEST :
				towords = IRobot.EAST;
				break;
			default :	
				possibleDirection = IRobot.CENTRE;
				towords = IRobot.EAST;
			}
			robot.setHeading(towords);
		}
		else
		{
			do
			{
				randomNo = gettingRandomNo(0, 3);
				possibleDirection = IRobot.AHEAD+randomNo;
			}while(robot.look(possibleDirection) != IRobot.PASSAGE);
		}

		return possibleDirection;
	}

	// called when reset button pressed 
	public void refresh()
	{
		robotRunning = 0;
		modes = 1;
		robotData.refreshJunction();
	}

	//getting the random no within the range
	private int gettingRandomNo(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}
}

/**
 * This class stores log of visited junctions
 */
class LogsOfRobot_25
{
	private static int visitedJuncNo;		// no. of junctions visited
	private static int totalJunctionAllow = 5000;
	private int[] theWayFromRobotCameToJunction;	
	// the Way From Robot Came To Junction
	
	//constructor
	public LogsOfRobot_25()
	{
		// Initialize variables
		visitedJuncNo = 0;
		theWayFromRobotCameToJunction = new int [totalJunctionAllow];
	}

	// reseting the junction from initial
	public void refreshJunction()
	{
		visitedJuncNo = 0;
	}

	//saving the current junction
	public void junctionLogs(int fronting)
	{
		theWayFromRobotCameToJunction [visitedJuncNo] = fronting;
		visitedJuncNo++;
	}

	//here we are searching the location of junction
	public int findingJunction()
	{
		if(visitedJuncNo <= 0) // here all junctions are visited
			return -1;	
		else
		{
			visitedJuncNo--;
			return theWayFromRobotCameToJunction[visitedJuncNo];	
		}
	}
}