import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_13
{

	private int randNum;
	private int direction;
	
	public void controlRobot(IRobot robot)
	{	
		isTargetNorth(robot);
		isTargetEast(robot);
		do
		{
			//Select a random number
			randNum = randomNumber(0, 3);
			// Convert this to a direction
			switch(randNum)
			{
			case 0:	direction = IRobot.LEFT;
				break;
			case 1:	direction = IRobot.RIGHT;
				break;
			case 2:	direction = IRobot.BEHIND;
				break;
			default:
				direction = IRobot.AHEAD;
			}
		}while(robot.look(direction)==IRobot.WALL);

		//keeps choosing a direction till it finds a non wall one
		// Face the robot in this direction
		robot.face(direction);  
		// Move the robot
		robot.advance();
	}


	//getting the random no within the range
	private int randomNumber(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}


	private int isTargetNorth(IRobot robot)
	{
		// returning 1 for ‘yes’, -1 for ‘no’ and 0 for ‘same level’
		if (robot.getLocationY() > robot.getTargetLocation().y )
		{
			System.out.println("Target is in the north direction.");
			return 1;
		}
		else if (robot.getLocationY() < robot.getTargetLocation().y )
		{
			System.out.println("Target is in the south direction.");
			return -1;
		}
		else
		{
			System.out.println("Target is at same level.");
			return 0;
		}
	}
	
	private int isTargetEast(IRobot robot)
	{
		// returning 1 for ‘yes’, -1 for ‘no’ and 0 for ‘same level’
		if (robot.getLocationX() > robot.getTargetLocation().x )
		{
			System.out.println("Target is in the East direction.");
			return 1;
		}
		else if (robot.getLocationX() < robot.getTargetLocation().x )
		{
			System.out.println("Target is in the West direction.");
			return -1;
		}
		else
		{
			System.out.println("Target is at same level.");
			return 0;
		}
	}
}