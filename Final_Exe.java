import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Final_Exe
{
	// step count in every step
	private int robotRunning = 0;		
	private RouteA robotA;
	//  zero for  backtrack and one  for explorer;
	private int mode;	
	// one for learn  and zero for gained 
	private int gained;		

	public void controlRobot(IRobot robot)
	{
		int line = IRobot.CENTRE;
		if ( robot.getRuns() == 0)
		{
			gained = 1;
			if (robotRunning == 0)
			{
				robotA = new RouteA(); 	// when new maze selected on first movement of first running
				mode = 1;				
			}
			if (mode == 1)	// first time meeting path is availale
				line = controllingExplore(robot);
			else						// No first time meeting path is availale
				line = controllingbacktrack(robot);
		}
		else
		{
			gained = 0;
			line = controlling(robot);
		}
		robot.face(line);  	// facing the robot at obtained direction

		if ( (availableWays(robot) > 2) && (gained == 1) )
			updateKnowledge(robot.getLocationX(), robot.getLocationY(), robot.getHeading(), 1);
		//robot is start to learn then we updating there knowledge
		robot.advance();	// moving the robot
		robotRunning++;		// Incrementing
	}

	// Controller for exploring mode
	public int controllingExplore(IRobot robot)
	{
		int line;
		// getting ways on the basis total no of ways available.
		int exits = availableWays(robot);

		// switching the path or way  on the asis of path availale at present location
		switch(exits)
		{
		case 1 : 
			line = atTheDead_Ends(robot);// only one way available for Dead_Ends
			break;
		case 2 : 
			line = atThe_Corridor(robot);// two ways available at Corridor 
			break;
		default : 
			line = atThe_crossRoadandjunction(robot);//four or three  ways are available
			break;
		}
		return line;
	}

	// Controller for backtracking mode
	public int controllingbacktrack(IRobot robot)
	{
		int line;
		// getting ways on the basis total no of ways available.
		int exits = availableWays(robot);

		// switching the path or way  on the asis of path availale at present location
		switch(exits)
		{
		case 1 : 
			line = atTheDead_Ends(robot);// only one way available for Dead_Ends
			break;
		case 2 : 
			if (toCheckNon_WallSides(robot) != 0)
				mode =1;		
			line = atThe_Corridor(robot); // two ways available at Corridor 
			break;
		default : 
			line = atThe_crossRoadandjunction(robot);//four or three  ways are available
		}
		return line;
	}
	// repeating run of maze
	public int controlling(IRobot robot)
	{
		int line;
		// getting ways on the basis total no of ways available.
		int exits = availableWays(robot);
		// switching the path or way  on the asis of path availale at present location
		switch(exits)
		{
		case 1 : 
			line = atTheDead_Ends(robot);// only one way available for Dead_Ends
			break;
		case 2 : 
			line = atThe_Corridor(robot);// two ways available at Corridor 
			break;
		default : 
			line = gettingGainedKnowledge(robot);//four or three  ways are available
		}
		return line;
	}
	//save junction when it repeating
	public int gettingGainedKnowledge(IRobot robot)
	{
		int line;
		// leading to Goal
		int turn = robotA.getTurnifPossible(robot.getLocationX(), robot.getLocationY());

		if (turn == -1)					// turning BEHIND
			line = IRobot.BEHIND;
		else						// towards to target 
			line = IRobot.AHEAD + turn;
		return line;
	}

	//from this function we can get total no of exits available
	private int availableWays(IRobot robot)
	{
		int availableExitsCount = 0;

		// from this loop we get availale path count
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) != IRobot.WALL)
				availableExitsCount++;
		}
		return availableExitsCount;
	}

	//getting count of non wall side of robot which is currently facing
	private int toCheckNon_WallSides(IRobot robot)
	{
		int non_WallSides = 0;
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) == IRobot.PASSAGE)
			{
				non_WallSides++;//incrementing passage count.
			}
		}
		return non_WallSides;
	}

	// getting the exact no when robot was previously visited at the current position
	private int exitsOnbeenbefore(IRobot robot)
	{
		int beenbeforeExitsCount = 0;

		// here we checking  for  no of previously visited  exits adjacent to current posiotion
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) == IRobot.BEENBEFORE)
			{
				beenbeforeExitsCount++;
			}
		}
		return beenbeforeExitsCount;
	}

	// checking conditions
	public int[] gaining(IRobot robot)
	{
		int[] CondintionCheck = new int[4];
		// checking conditions and save  
		for (int side = 0; side<4; side++)
		{
			switch(robot.look(IRobot.AHEAD+side))
			{
			case IRobot.BEENBEFORE :
				CondintionCheck[side] = 0;
				break;
			case IRobot.PASSAGE :
				CondintionCheck[side] = -1;
				break;
			case IRobot.WALL :
				CondintionCheck[side] = -100;
				break;
			}
		}
		return CondintionCheck;
	}

	//getting the reverse the facing 
	public int reversing(int facing)
	{
		int reverseFacing;
		// Reverse robot facing
		switch(facing)
		{
		case IRobot.NORTH :
			reverseFacing = IRobot.SOUTH ;
			break;
		case IRobot.EAST :
			reverseFacing = IRobot.WEST;
			break;
		case IRobot.SOUTH :
			reverseFacing = IRobot.NORTH;
			break;
		default :
			reverseFacing = IRobot.EAST;
		}
		return reverseFacing;
	}

	//getting the updated knowledge of junction to current 
	public void updateKnowledge(int locX, int locY, int fronting, int values)
	{
		int subscript = 0;

		//one for path currently selected zero for wrong and visited path 
		int cameFrom = robotA.findingJunction(locX, locY);

		// zero for reversing
		if(values == 0)
			fronting = reversing(fronting);
		// fronting to initial fronting
		switch(cameFrom)
		{
		case IRobot.NORTH :
			subscript = 0;
			break;
		case IRobot.EAST :
			subscript = -1;
			break;
		case IRobot.SOUTH :
			subscript = -2;
			break;
		case IRobot.WEST :
			subscript = -3;
			break;
		}

        // going through directions getting randomly one according to 0/1
		for(int i = subscript; i<(4+subscript); i++)
		{
			if( (cameFrom+i) == fronting )
				robotA.updatingJunc(locX, locY, ((4+i)%4), values);
		}
	}
	
	// when robot is at DeadEnd;
	private int atTheDead_Ends(IRobot robot)
	{
		int possibleDirection = IRobot.AHEAD;

		if (robotRunning == 0)
		{	
			for(int i = 1; i<=3 && ( robot.look(possibleDirection)==IRobot.WALL) ; i++)
			{
				possibleDirection = IRobot.AHEAD+i; //incrementing  
			}
		}
		else
		{	
			mode = 0;
			possibleDirection = IRobot.BEHIND;// turning
		}
		return possibleDirection;
	}

	//when robot is at corridor
	private int atThe_Corridor(IRobot robot)
	{
		int possibleDirection = IRobot.AHEAD;

		if (robot.look(IRobot.AHEAD) != IRobot.WALL)
		{
			possibleDirection = IRobot.AHEAD;//  AHEAD
		}
		else if (robot.look(IRobot.LEFT) != IRobot.WALL)
		{
			possibleDirection = IRobot.LEFT;  // turning LEFT
		}
		else if (robot.look(IRobot.RIGHT) != IRobot.WALL)
		{
			possibleDirection = IRobot.RIGHT;    // turning RIGHT
		}
		return possibleDirection;
	}

	// when robot is at  cross_road or junction
	private int atThe_crossRoadandjunction(IRobot robot)
	{
		int possibleDirection = IRobot.AHEAD;
		int exits = toCheckNon_WallSides(robot);		
		int count  = exitsOnbeenbefore(robot);		

		int locX = robot.getLocationX();
		int locY = robot.getLocationY();
		int front = robot.getHeading();

		if (count == 1)		// first time meeting path is availale			
		{
			int[] surroundingConditions = gaining(robot);
			robotA.junctionLogs(locX, locY, front, surroundingConditions);
		}
		else if (count > 1)
		{
			//robot updating the knowledge   setting to zero
			updateKnowledge(locX, locY, front, 0);
		}

		if (exits == 0)
			mode = 0;	// first time meeting path is availale
		else
			mode = 1;	// No first time meeting path is availale
		if (mode == 0)//// Backtracking
		{
			int arrivedFrom = robotA.findingJunction(locX, locY);
			int goTowards = reversing(arrivedFrom);
			robot.setHeading(goTowards);
		}
		else
		{
			// randomly selecting
			int randomNum;
			do
			{
				randomNum = gettingRandomNo(0, 3);
				possibleDirection = IRobot.AHEAD+randomNum;
			}while(robot.look(possibleDirection) != IRobot.PASSAGE);
		}

		return possibleDirection;
	}

	// called when reset button pressed 
	public void reset()
	{
		robotRunning = 0;
		mode = 1;
	}

	//getting the random no within the range
	private int gettingRandomNo(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}
}


/**
 * This class stores log of visited junctions
 */
class RouteA
{
	private static int totalJunctionAllow = 10000;
	private static int visitedJuncNo;		
	private static LogsOfJunction logsJunction[];

	//  inner-class to save junction data
	private class LogsOfJunction
	{
		private int xJunction;				
		private int yJunction;				
		private int cameFrom;				
		private int[] checkCond;

	//  inner-class constructor
		public LogsOfJunction(int locX, int locY, int fronting, int[] checkConditionss)
		{
			xJunction = locX;
			yJunction = locY;
			cameFrom = fronting;
			this.checkCond = new int[4];
			for(int i = 0; i<4; i++)
			{
				this.checkCond[i] = checkConditionss[i];
			}
		}
	}

	//constructor
	public RouteA()
	{
		visitedJuncNo = 0;
		logsJunction = new LogsOfJunction [totalJunctionAllow];
	}

	// Saves log of unencountered junction robot is visiting
	public void junctionLogs(int locX, int locY, int fronting, int[] conditions)
	{
		logsJunction[visitedJuncNo] = new LogsOfJunction(locX, locY, fronting, conditions);
		showJunction();
		visitedJuncNo++;
	}

	/// Prints log of current junction robot is visiting
	public void showJunction()
	{
		String fronting = "";

		switch(logsJunction[visitedJuncNo].cameFrom)
		{
		case IRobot.NORTH :	fronting = "NORTH";
		break;
		case IRobot.EAST :	fronting = "EAST";
		break;
		case IRobot.SOUTH :	fronting = "SOUTH";
		break;
		case IRobot.WEST :	fronting = "WEST";
		break;
		}

		//printing the logs of junction
		System.out.print(" Position X = "+ logsJunction[visitedJuncNo].xJunction);
		System.out.print( ", Position Y = " + logsJunction[visitedJuncNo].yJunction);
		System.out.print(" Junction Visited " + (visitedJuncNo+1) );
		System.out.print(" fronting  " + fronting);
		System.out.println();
	}


	//here we are searching the location of junction
	public int findingJunction(int loxX, int locY)
	{
		for(int i = 0; i<visitedJuncNo; i++)	// each junction who already visited
		{
			if ( (logsJunction[i].xJunction == loxX) && (logsJunction[i].yJunction == locY) )
			{	
				return logsJunction[i].cameFrom;
			}
		}
		//if record not found
		return -1;	
	}

	public void updatingJunc(int locX, int locY, int situation, int value)
	{
		for(int i = 0; i<visitedJuncNo; i++)	// each junction who already visited
		{
			if ( (logsJunction[i].xJunction == locX) && (logsJunction[i].yJunction == locY) )
			{
				logsJunction[i].checkCond[situation] = value;
				break;
			}
		}
	}

	
	public int getTurnifPossible(int xPos, int yPos)
	{
		for(int j = 0; j<visitedJuncNo; j++)	// each junction who already visited
		{
			
			if ( (logsJunction[j].xJunction == xPos) && (logsJunction[j].yJunction == yPos) )
			{
				for(int i = 0; i<4; i++)	//all directions
				{
					if (logsJunction[j].checkCond[i] == 1)
						return i;
				}
			}
		}
		return -1;
	}
}