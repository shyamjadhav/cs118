import uk.ac.warwick.dcs.maze.logic.IRobot;

/**
 * @author tailoredtech 
 * Printing the logs of robot
 *
 */

public class Exercise_20
{
	// step count in every step
	private int robotRunning = 0;		
	// when new maze selected on first movement of first running
	private LogsOfRobot_20 logsOfRobot;

	//from this function we can get total no of exits available
	private int availableWays(IRobot robot)
	{
		int availableExitsCount = 0;
		// from this loop we get availale path count 
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) != IRobot.WALL)
			{
				availableExitsCount++;
			}
		}
		return availableExitsCount;
	}

	// when robot is at  cross_road or junction
	private int atThe_crossRoadandjunction(IRobot robot)
	{
		int possibleDirection;
		int availableExits = toCheckNon_WallSides(robot);
		int randomNum;
		previouslyVisited(robot);
		logsOfRobot.visitedJunction(robot.getLocationX(), robot.getLocationY(), robot.getHeading());

		if (availableExits == 0)
			possibleDirection = anyWayToExitsFromNonWall(robot);
		else
		{	do
		{
			randomNum = gettingRandomNo(0, 3);
			possibleDirection = IRobot.AHEAD+randomNum;

		}while(robot.look(possibleDirection) != IRobot.PASSAGE);
		}
		return possibleDirection;
	}

	//main method to to run robot
	public void controlRobot(IRobot robot)
	{
		int line;
		int waysToExits = availableWays(robot);
		if ( (robot.getRuns() == 0) && (robotRunning == 0))
		{
			logsOfRobot = new LogsOfRobot_20();
			logsOfRobot.initialize();
		}
		robotRunning++;	
		// getting ways on the basis total no of ways available.
	
		// switching the path or way  on the asis of path availale at present location
		switch(waysToExits)
		{
		case 1 : 
			line = atTheDead_Ends(robot);// only one way available for Dead_Ends
			break;
		case 2 : 
			line = atThe_Corridor(robot);// two ways available at Corridor 
			break;
		default : 
			line = atThe_crossRoadandjunction(robot);//four or three  ways are available
			break;
		}
		// facing the robot at obtained direction
		robot.face(line);  
		// moving the robot
		robot.advance();
	}

	//getting count of non wall side of robot which is currently facing 
	private int toCheckNon_WallSides(IRobot robot)
	{
		int non_WallSides = 0;
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) == IRobot.PASSAGE)
				non_WallSides++;  //incrementing passage count.
		}
		return non_WallSides;
	}

	// when robot is at DeadEnd;
	private int atTheDead_Ends(IRobot robot)
	{
		int possibleWay;
		if(robot.getLocationX() == 1 && robot.getLocationY() == 1)
			possibleWay = anyWayToExitsFromNonWall(robot);
		else
			possibleWay = IRobot.BEHIND;

		return possibleWay;
	}

	//when robot is at corridor
	private int atThe_Corridor(IRobot robot)
	{
		int getPossibleDirection = IRobot.AHEAD;

		if (robot.look(IRobot.AHEAD) != IRobot.WALL)
			getPossibleDirection = IRobot.AHEAD;

		else if (robot.look(IRobot.LEFT) != IRobot.WALL)
			getPossibleDirection = IRobot.LEFT;

		else if (robot.look(IRobot.RIGHT) != IRobot.WALL)
			getPossibleDirection = IRobot.RIGHT;

		return getPossibleDirection;
	}

	// get any way to exit non wall
	private int anyWayToExitsFromNonWall(IRobot robot)
	{
		int possibleDirection;
		int randNo;
		do
		{
			randNo = gettingRandomNo(0,3);
			possibleDirection = IRobot.AHEAD+randNo;
		}while(robot.look(possibleDirection)==IRobot.WALL);
		return possibleDirection;
	}

	// called when reset button pressed 
	public void refresh()
	{
		robotRunning = 0;
		LogsOfRobot_20.visitedJuncNo = 0;
	}

	//getting the random no within the range
	private int gettingRandomNo(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}

	// getting the exact no when robot was previously visited at the current position
	private int previouslyVisited(IRobot robot)
	{
		int previouslyVisited = 0;
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) == IRobot.BEENBEFORE)
				previouslyVisited++;
		}
		return previouslyVisited;
	}

}

/**
 * @author tailoredtech 
 * Printing the logs of robot 
 */

class LogsOfRobot_20
{
	public static int visitedJuncNo;		
	private static int totalJunctionAllow = 5000;

	private int[] xJunction;				
	private int[] yJunction;				
	private int[] theWayFromRobotCameToJunction;
	// the Way From Robot Came To Junction

	// here we getting the already visited  Junction
	public void visitedJunction(int locX, int locY, int front)
	{
		xJunction [visitedJuncNo] = locX;
		yJunction [visitedJuncNo] = locY;
		theWayFromRobotCameToJunction [visitedJuncNo] = front;
		junctionLogs();
		visitedJuncNo++;
	}

	// initializing the variales here 
	public void  initialize()
	{
		visitedJuncNo = 0;
		xJunction = new int [totalJunctionAllow];
		yJunction = new int [totalJunctionAllow];
		theWayFromRobotCameToJunction = new int [totalJunctionAllow];
	}

	//showing the current junction
	public void junctionLogs()
	{
		String fronting = "";
		switch(theWayFromRobotCameToJunction[visitedJuncNo])
		{
		case IRobot.NORTH :	fronting = "NORTH";
		break;
		case IRobot.EAST :	fronting = "EAST";
		break;
		case IRobot.SOUTH :	fronting = "SOUTH";
		break;
		case IRobot.WEST :	fronting = "WEST";
		break;
		}

		// printing the all positions
		System.out.print(" Position X = " + xJunction[visitedJuncNo]);
		System.out.print( ", Position Y = " + yJunction[visitedJuncNo]);
		System.out.print(" Junction Visited " + (visitedJuncNo+1) );
		System.out.print(" fronting  " + fronting);
		System.out.println();

	}
}