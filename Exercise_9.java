import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Exercise_9
{
	int randNo;
	int line;
	int count;
	String head;
	int steps  = 0;
	int maximumSteps;

	public void controlRobot(IRobot robot)
	{
		if(steps == 0)
			maximumSteps = randomNumber(1, 8);	
		// getting random no in between 1 to 8.
		steps++;
		head = "forward";
		line = IRobot.AHEAD;


		while( ( robot.look(line) == IRobot.WALL) || (steps == maximumSteps) )
		{
			steps = 0;

			randNo = randomNumber(0, 3);
			// getting random no in between 0 to 3 to select direction.
			if (randNo == 0)
			{
				line = IRobot.LEFT;
				head = "left";
			}
			else if (randNo == 1)
			{
				line = IRobot.RIGHT;
				head = "right";
			}
			else if (randNo == 2)
			{
				line = IRobot.BEHIND;
				head = "ackwards";
			}
			else {
				line = IRobot.AHEAD;
				head = "forward";
			}
			
		}

		// facing the robot in given direction 
		robot.face(line);

		robotFacing(robot);
		System.out.println("I am going "+head);

		//and move the robot
		robot.advance();
	}

	private void robotFacing(IRobot robot)
	{
		count = 0;
		for (int side = 0; side<4; side++)
		{
			if(robot.look(IRobot.AHEAD+side) == IRobot.WALL)
			{
				count++;
			}
		}
		//getting location
		switch(count)
		{
		case 0:	head += " at crossroads.";
		break;
		case 1:	head += " at a junction.";
		break;
		case 2:	head += " down a corridor.";
		break;
		default: head += " at a deadend.";
		}
	}

	// getting random num. in the range of min-max
	private int randomNumber(int min, int max)
	{
		return (int) Math.floor( Math.random() * (max - min + 1) + min);
	}

}
